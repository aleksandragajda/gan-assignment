$(document).ready(function() {

  deleteFile();
  selectAllItems();

});

function deleteFile() {

  var $deleteButton = $('.gn-delete');
  var $modal = $('.gn-modal');
  var $modalYes = $('.gn-modal__answer--yes');
  var $modalNo = $('.gn-modal__answer--no');
  var $modalClose = $('.gn-modal__close-button');


  $deleteButton.on('click', function() {


    //
    // Check if any of checkboxes are checked
    //

    if ( $('input:checkbox:checked').length > 0 ) {
      $modal.fadeIn( 300 );
    }

  });

  //
  // Animate and delete row on click YES
  //

  $modalYes.on('click', function() {

    var $file = $('.gn-files__row');

    $file.each (function(index) {

      var $checkbox = $(this).find('input');

      if ($checkbox.is(':checked')) {
        $(this).addClass('gn-delete-file').delay( 300 ).slideUp( 300 ).delay( 300 ).remove();
      }
      $modal.fadeOut( 300 );


    });
  });

  //
  // Close modal on click NO or X-button
  //

  $modalNo.on('click', function() {
    $modal.fadeOut( 300 );
  });

  $modalClose.on('click', function() {
    $modal.fadeOut( 300 );
  });


}

function selectAllItems() {

  var $selectAll = $('.gn-file-order__select-all .gn-checkbox--all')
  var $checkbox = $('.gn-checkbox');

  $selectAll.change(function(){
    var status = $(this).is(':checked');
    console.log(status);
    $('.gn-checkbox').each(function(){
      this.checked = status;
    });
  });

  //
  //uncheck "select all", if one of the listed checkbox item is unchecked
  //

  $checkbox.change(function(){

    if( !($(this).is(':checked')) ){
      $selectAll[0].checked = false;
    }

  });
}
